"""HelloWorld URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

# urlpatterns = [
#     path('admin/', admin.site.urls),
# ]

from django.conf.urls import url
 
from . import views
#      'url': request.get_host() + new_path,
# RuntimeError: You called this URL via POST, but the URL doesn't end in a slash and you have APPEND_SLASH set. Django can't redirect to the slash URL while maintaining POST data. Change your form to point to localhost:8000/post/ (note the trailing slash), or set APPEND_SLASH=False in your Django settings.
# [28/Aug/2022 21:19:43] "POST /post HTTP/1.1" 500 71301

from django.contrib import admin
from django.urls import path
# from blog.views import *
# https://blog.csdn.net/kimheesunliulu/article/details/100690975
urlpatterns = [
       path('admin/', admin.site.urls),
    url(r'^$', views.hello),
    # 没有最后的 $ 是不行的 
      url(r'^index$', views.index),
    #   url(r'^post/$',views.params_post),
        url(r'^post$',views.params_post),
           url(r'^test$',views.testView),
             url(r'^themes$',views.themes),
               url(r'^themes_get_by_id$',views.themes_get_by_id),
     url(r'^coder_insert$',views.coder_insert),
        url(r'^find_one$',views.find_one),
         url(r'^getDatas$',views.getDatas),
              url(r'^ReplaceSelectTest$',views.ReplaceSelectTest),
                  url(r'^find_one_by_jobTitle$',views.find_one_by_jobTitle),
         
               
]

#     'url': request.get_host() + new_path,
# RuntimeError: You called this URL via POST, but the URL doesn't end in a slash and you have APPEND_SLASH set. Django can't redirect to the slash URL while maintaining POST data. Change your form to point to localhost:8000/post/ (note the trailing slash), or set APPEND_SLASH=False in your Django settings.

# RuntimeError: You called this URL via POST, but the URL doesn't end in a slash and you have APPEND_SLASH set. Django can't redirect to the slash URL while maintaining POST data. Change your form to point to localhost:8000/post/ (note the trailing slash), or set APPEND_SLASH=False in your Django settings.